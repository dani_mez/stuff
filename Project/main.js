/**
 * Created by S on 2015.03.26..
 *
 * TODO: 6. property mikéntje?
 *
 */

var http = require("http");
var url = require('url');
var fs = require('fs');
var io = require('socket.io')(http);


var server = http.createServer(function(request, response){
    console.log('Connection');

    var path = url.parse(request.url).pathname;
    console.log(path)


    if(path == '/Wallman.html'){
        fs.readFile(__dirname + '/Wallman.html', function(err, data){

            if(err){
                response.end("Problem occurred, during loading the page!")
            }
            else{
                response.end(data)
            }
        })

    }

    if(path == '/Wallman.css'){
        fs.readFile(__dirname + '/Wallman.css', function(err, data){

            if(err){
                response.end("Problem occurred, during loading the page!")
            }
            else{
                response.end(data)
            }
        })

    }

});

server.listen(8001);

io.listen(server);

var players = {};
var takenNames = [];

var WIDTH = 1020;
var HEIGHT = 510;
var dx = 30;
var dy = 30;

function Player (name){
    this.name = name;
    this.x = 30;
    this.y = 30;
    this.direction = "down";
    this.availableBombs = 3;
    this.bombs = [];
    this.walls = [];
}

function Bomb(id, x, y){
    this.playerId = id;
    this.x = x;
    this.y = y;
    var explode = function(duration) {
        setTimeout(function () {
            if (players[id] === undefined) return;
            players[id].bombs.splice(0, 1);
            players[id].walls.push(new Wall(id, x, y));
            players[id].walls.push(new Wall(id, x, y + 30));
            players[id].walls.push(new Wall(id, x, y + 60));
            players[id].walls.push(new Wall(id, x + 30, y));
            players[id].walls.push(new Wall(id, x + 60, y));
            players[id].walls.push(new Wall(id, x, y - 30));
            players[id].walls.push(new Wall(id, x, y - 60));
            players[id].walls.push(new Wall(id, x - 30, y));
            players[id].walls.push(new Wall(id, x - 60, y));

            checkTerritories(id);
        }, duration)
    }
    explode(5000);
}

function Wall(id, x, y){
    this.playerId = id;
    this.x = x;
    this.y = y;
}

function checkTerritories(id){
    
}

function disconnectedPlayer(socketid){
    if(players[socketid] == null) return;
    delete players[socketid];
}

io.on('connection', function(socket){

    if(players.length > 2) return;

    turns(socket);

    var currentNumber = 1;
    for(;currentNumber <= 3; currentNumber++){
        if(takenNames.length == 0) break;
        if(takenNames.indexOf("player" + currentNumber) > -1) continue;
        break;
    }

    var player = "player" + currentNumber;
    takenNames.push(player);

    players[socket.id] = new Player(player);

    console.log(player);

    // Send everyone to the new
    for (var key in players) {
        if (players.hasOwnProperty(key)) {
            socket.emit('addPlayer', {id: key});
        }
    }
    socket.on('playerAdded', function (data) {
        var key = data.id;
        sendPicture("down", players[key].name, socket, key);
        sendPicture("up", players[key].name, socket, key);
        sendPicture("left", players[key].name, socket, key);
        sendPicture("right", players[key].name, socket, key);
        sendPicture("bomb1", players[key].name, socket, key);
        sendPicture("bomb2", players[key].name, socket, key);
        sendPicture("wall", players[key].name, socket, key);
    });

    // Send the new to everyone

    io.emit('addPlayer', {id: socket.id});




    socket.on('keydown', function (data) {
        keyPressed(socket, data);
    });

    socket.on('disconnect', function() {
        console.log('Got disconnect!');
        takenNames.splice(takenNames.indexOf(players[socket.id].name), 1);
        disconnectedPlayer(socket.id);

        io.emit('deletePlayer', {id: socket.id});
    });

    fs.readFile(__dirname + "/pics/" + "wall.png", function (err, buf) {
        if (err) {
            console.log(err.toString());
        }
        else {
            socket.emit('basicWall', {image: true, buffer: buf.toString('base64')});
        }
    })

});

var keyPressed = function(socket, data){
    switch (data.keyCode) {
        case 39: // Arrow to the right
            if (players[socket.id].x + dx < WIDTH-60){
                players[socket.id].direction = "right";

                if(canMove(socket.id, "right")){
                    players[socket.id].x += dx;
                }
            }
            break;
        case 40: // Arrow to the down
            if (players[socket.id].y + dy < HEIGHT-70){
                players[socket.id].direction = "down";

                if(canMove(socket.id, "down")){
                    players[socket.id].y += dy;
                }

            }
            break;
        case 37: // Arrow to the left
            if (players[socket.id].x - dx > 0){
                players[socket.id].direction = "left";

                if(canMove(socket.id, "left")){
                    players[socket.id].x -= dx;
                }


            }
            break;
        case 38: // Arrow to the up
            if (players[socket.id].y - dy > 0){
                players[socket.id].direction = "up";

                if(canMove(socket.id, "up")){
                    players[socket.id].y -= dy;
                }

            }
            break;
        case 32: // Arrow to the up
            if (players[socket.id].availableBombs > 0){
                var bomb = new Bomb(socket.id, players[socket.id].x, players[socket.id].y)
                players[socket.id].bombs.push(bomb);
                players[socket.id].availableBombs--;
            }
            break;

    }
}

function canMove(id, direction){

    for (var key in players) {

        if (players.hasOwnProperty(key) && key != id) {

            for( var i = 0; i < players[key].walls.length; i++){
                switch(direction){
                    case "right":

                        if(players[id].y <= players[key].walls[i].y + 20 &&
                            players[id].y >= players[key].walls[i].y - 40 &&
                            players[id].x <= players[key].walls[i].x - 30 &&
                            players[id].x >= players[key].walls[i].x - 60) return false;
                        break;

                    case "down":
                        if(players[id].x <= players[key].walls[i].x + 20 &&
                            players[id].x >= players[key].walls[i].x - 40 &&
                            players[id].y <= players[key].walls[i].y - 30 &&
                            players[id].y >= players[key].walls[i].y - 60) return false;
                        break;

                    case "left":
                        if(players[id].y <= players[key].walls[i].y + 20 &&
                            players[id].y >= players[key].walls[i].y - 40 &&
                            players[id].x >= players[key].walls[i].x + 20 &&
                            players[id].x <= players[key].walls[i].x + 50) return false;
                        break;

                    case "up":
                        if(players[id].x <= players[key].walls[i].x + 20 &&
                            players[id].x >= players[key].walls[i].x -40 &&
                            players[id].y >= players[key].walls[i].y + 20 &&
                            players[id].y <= players[key].walls[i].y + 50) return false;
                        break;
                }
            }
        }
    }
    return true;
}

var sendPicture = function (picname, player, socket, socketid) {

    fs.readFile(__dirname + "/pics/" + player + "/" + picname + ".png", function (err, buf) {
        if (err) {
            console.log(err.toString());
        }
        else {
            socket.emit('image', {image: true, socketid:socketid, player: player ,direction: picname, buffer: buf.toString('base64')});
        }
    })
}

// Broadcast:
var turns = function (socket){
    setInterval(function(){
        socket.emit('status', players);
    }, 20);
}

// Every player get a bomb in every 10 sec
var getNewBombs = function (duration){
    setInterval(function(){
        for (var key in players) {
            if (players.hasOwnProperty(key)) {
                if (players[key].availableBombs < 5) players[key].availableBombs++;
            }
        }
    }, duration);
}

getNewBombs(5000);

